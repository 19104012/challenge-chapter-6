const carsService = require("../../../services/carsService");

const checkUser = (user) => {
  let message;
  if (user.role == "superadmin" || user.role == "admin") {
    message = {
      status: true,
      message: "You are authorized to access this resource",
    };
  } else {
    message = {
      status: false,
      message: "You are not authorized to access this resource",
    };
  }
  return message;
};

const create = async (req, res, next) => {
  const { name, price, size, available } = req.body;
  const role = req.user;
  const checkedUser = await checkUser(role);
  if (!checkedUser.status) return res.status(401).json(checkedUser);
  const { status, status_code, message, data } = await carsService.create({
    name,
    price,
    size,
    available,
    createdBy: req.user.email,
  });

  res.status(status_code).send({
    status: status,
    message: message,
    data: data,
  });
};

const getAll = async (req, res) => {
  const { status, status_code, message, data } = await carsService.getAll();

  res.status(status_code).send({
    status: status,
    message: message,
    data: data,
  });
};

const deleteCars = async (req, res, next) => {
  const { id } = req.params;
  const role = req.user;
  const checkedUser = await checkUser(role);
  if (!checkedUser.status) return res.status(401).json(checkedUser);
  const deletedBy = req.user.name;
  const { status, status_code, message, data } = await carsService.deleteCars({
    id,
    deletedBy: req.user.email,
  });

  res.status(status_code).send({
    status: status,
    message: message,
    data: data,
  });
};

const updateCars = async (req, res, next) => {
  const { id } = req.params;
  const { name, price, size, available } = req.body;
  const role = req.user;
  const checkedUser = await checkUser(role);
  if (!checkedUser.status) return res.status(401).json(checkedUser);
  const { status, status_code, message, data } = await carsService.updateCars({
    id,
    name,
    price,
    size,
    available,
    updatedBy: req.user.email,
  });

  res.status(status_code).send({
    status: status,
    message: message,
    data: data,
  });
};

const getByAvailable = async (req, res) => {
  const { available } = req.body;

  const { status, code_status, message, data } =
    await carsService.getByAvailable({ available });

  res.status(code_status).send({
    status: status,
    message: message,
    data: data,
  });
};
module.exports = { create, getAll, deleteCars, updateCars, getByAvailable };
