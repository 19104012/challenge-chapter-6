/**
 * @file Bootstrap express.js server
 * @author Fikri Rahmat Nurhidayat
 */

 const express = require("express")
 const bodyParser = require("body-parser")
 const router = require("../config/routes")
 const cors = require("cors")
 const YAML = require('yamljs')
 const swaggerUI = require('swagger-ui-express')
 const apiDocs = YAML.load('./api-docs.yaml')
 const app = express()
 
 app.use(cors())
 app.use(bodyParser.urlencoded({ extended: true }))
 app.use(bodyParser.json())
 app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(apiDocs));
 app.use(router)
 
 module.exports = app