const express = require("express");
const path = require("path");
const controllers = require("../app/controllers");
const authController = controllers.api.v1.authController;
const usersController = controllers.api.v1.usersController;
const carsController = controllers.api.v1.carsController;
const apiRouter = express.Router();

// Melihat seluruh user pada database
apiRouter.get("/api/v1/users", usersController.getUsers);

// Memnambahkan member pada database
apiRouter.post("/api/v1/register/member", usersController.register);

// menambahkan admin (superadmin) pada database
apiRouter.post(
  "/api/v1/register/admin",
  authController.authorize,
  usersController.registerAdmin
);

// Melakukan login
apiRouter.post("/api/v1/login", authController.login);

// Melakukan update & delete user
apiRouter
  .route("/api/v1/user/:id")
  .put(usersController.update)
  .delete(usersController.deleteUser);

// Mendapatkan user yang login
apiRouter.get(
  "/api/v1/user",
  authController.authorize,
  usersController.getCurrentUser
);

// Cars
apiRouter.post("/api/v1/cars/create", authController.authorize, carsController.create);
apiRouter.get("/api/v1/cars", authController.authorize, carsController.getAll);
apiRouter.put(
  "/api/v1/cars/update/:id",
  authController.authorize,
  carsController.updateCars
);
apiRouter.delete(
  "/api/v1/cars/delete/:id",
  authController.authorize,
  carsController.deleteCars
);
apiRouter.get("/api/v1/cars/available", carsController.getByAvailable);

module.exports = apiRouter;
